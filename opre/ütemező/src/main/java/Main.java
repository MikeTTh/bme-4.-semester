import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Task> allTasks = Task.parseFromStdin();
        ArrayList<Task> tasksToSchedule = new ArrayList<>(allTasks);
        
        ArrayList<Task> ran = new ArrayList<>();
        int time = 0;

        ShortestJobFirst sjf = new ShortestJobFirst();
        RoundRobin rr = new RoundRobin();

        boolean needsRun = true;
        while (!tasksToSchedule.isEmpty() || needsRun) {
            needsRun = true;
            ArrayList<Task> schedulable = new ArrayList<>(tasksToSchedule);
            for(Task t: schedulable) {
                if (t.start <= time) {
                    tasksToSchedule.remove(t);
                    if (t.prio == 1) {
                        sjf.addTask(t);
                    } else {
                        rr.addTask(t);
                    }
                }
            }
            
            Task ranTask = null;
            if (sjf.hasTasks()) {
                ranTask = sjf.scheduleTask();
                rr.waitTasks();
            } else if (rr.hasTasks()) {
                ranTask = rr.scheduleTask();
            } else {
                needsRun = false;
            }
            
            if (ranTask != null) {
                ran.add(ranTask);
            }
            time++;
        }

        Task lastPrinted = null;
        for (Task t: ran) {
            if (t != lastPrinted) {
                System.out.print(t.id);
            }
            lastPrinted = t;
        }
        System.out.println();
        int i = 0;
        for (Task t: allTasks) {
            i++;
            System.out.printf("%s:%d", t.id, t.wait);
            if (i != allTasks.size()){
                System.out.print(",");
            }
        }
    }
}
