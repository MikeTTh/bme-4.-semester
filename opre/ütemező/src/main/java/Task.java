import java.util.ArrayList;
import java.util.Scanner;

public class Task {
    public final String id;
    public final int prio;
    public final int start;
    public final int cpuTime;
    public int left;
    public int wait = 0;
    
    public Task(String id, int prio, int start, int cpuTime) {
        this.id = id;
        this.prio = prio;
        this.start = start;
        this.cpuTime = cpuTime;
        this.left = cpuTime;
    }
    
    public static Task parse(String line) {
        String[] parts = line.split(",");
        return new Task(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
    }
    
    public static ArrayList<Task> parseFromStdin(){
        ArrayList<Task> tasks = new ArrayList<>();
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine()){
            String line = scan.nextLine();
            if (!line.trim().equals("")) {
                tasks.add(parse(line));
            }
        }
        return tasks;
    }
}
