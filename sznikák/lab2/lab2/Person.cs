using System;
using System.Xml.Serialization;

namespace lab2
{
    public delegate void AgeChangingDelegate(int oldAge, int newAge);
    
    [XmlRoot("Személy")]
    public class Person
    {
        private int age;
        public event AgeChangingDelegate AgeChanging;

        [XmlAttribute("Kor")]
        public int Age
        {
            get { return age; }
            set
            {
                if (value < 0)
                    throw new Exception("Érvénytelen életkor!");
                AgeChanging?.Invoke(age, value);
                age = value;
            }
        }
        
        [XmlIgnore]
        public String Name { get; set; }
        
        
    }
}