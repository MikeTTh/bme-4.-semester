import java.util.ArrayList;

public class ShortestJobFirst {
    private final ArrayList<Task> tasks = new ArrayList<>();
    private Task lastTask = null;

    public void addTask(Task task) {
        tasks.add(task);
    }

    public Task scheduleTask() {
        Task shortest = tasks.get(0);
        for (Task t: tasks) {
            if (shortest.cpuTime > t.cpuTime) {
                shortest = t;
            } else if (shortest.cpuTime == t.cpuTime && shortest.id.compareTo(t.id) > 0) {
                shortest = t;
            }
        }
        
        if (tasks.contains(lastTask)) {
            shortest = lastTask;
        }

        shortest.left--;
        if (shortest.left <= 0) {
            tasks.remove(shortest);
        }

        for (Task t: tasks) {
            if (t != shortest) {
                t.wait++;
            }
        }

        lastTask = shortest;
        
        return shortest;
    }
    
    public boolean hasTasks() {
        return !tasks.isEmpty();
    }
}
