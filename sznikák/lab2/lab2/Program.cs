﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;

namespace lab2
{
    class Program
    {
        static void PersonAgeChanging(int oldAge, int newAge)
        {
            Console.WriteLine($"{oldAge} => {newAge}");
        }

        static bool MyFilter(int i)
        {
            return i % 2 == 1;
        }
        
        static void Main(string[] args)
        {
            Person p = new Person();
            p.AgeChanging += PersonAgeChanging;
            p.AgeChanging += PersonAgeChanging;
            p.AgeChanging -= PersonAgeChanging;
            p.Age = 2;
            p.Age++;
            p.Name = "Luke";
            Console.WriteLine(p.Name);
            
            XmlSerializer serializer = new XmlSerializer(typeof(Person));
            FileStream stream = new FileStream("person.txt", FileMode.Create);
            serializer.Serialize(stream, p);
            stream.Close();
            // Process.Start("kate", "person.txt");
            
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list = list.FindAll(MyFilter);
            foreach (int i in list)
            {
                Console.WriteLine($"Value: {i}");
            }
        }
    }
}