public class Frame {
    private int usedAt = Integer.MIN_VALUE;
    private final String id;
    private int page = -1;
    private int loadedAt = Integer.MIN_VALUE;

    public Frame(String id) {
        this.id = id;
    }

    public int getUsedAt() {
        return usedAt;
    }
    public boolean access(int time, int page){
        if (this.page != page)
            return false;

        usedAt = time;
        return true;
    }

    public void load(int time, int page) {
        usedAt = time;
        this.page = page;
        loadedAt = time;
    }
    public int getLoadedAt(){
        return loadedAt;
    }

    @Override
    public String toString(){
        return id;
    }
}
