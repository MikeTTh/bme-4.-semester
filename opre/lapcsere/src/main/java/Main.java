import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static ArrayList<Frame> frames = new ArrayList<>();

    public static void main(String[] args) {
        for (char c = 'A'; c <= 'C'; c++) {
            frames.add(new Frame(Character.toString(c)));
        }

        StringBuilder strb = new StringBuilder();
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextLine()) {
            strb.append(scan.nextLine());
        }
        String str = strb.toString().trim();
        int[] parts = Arrays.stream(str.split(",")).mapToInt(Integer::parseInt).toArray();

        int t = 0;
        int faults = 0;
        for (int i: parts) {
            i = Math.abs(i);
            boolean inMem = false;
            for (Frame f: frames) {
                if(f.access(t, i)) {
                    inMem = true;
                    break;
                }
            }
            if (inMem){
                System.out.print("-");
            } else {
                Frame lru = null;
                for (Frame f: frames) {
                    if (f.getLoadedAt() >= t-3) {
                        continue;
                    }

                    if (lru == null || f.getUsedAt() < lru.getUsedAt()) {
                        lru = f;
                    }
                }
                faults++;
                if (lru == null) {
                    System.out.print("*");
                } else {
                    lru.load(t, i);
                    System.out.print(lru.toString());
                }
            }
            t++;
        }
        System.out.println();
        System.out.println(faults);
    }
}
