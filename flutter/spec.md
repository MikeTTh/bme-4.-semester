# Leírás
Az app a COVID pandémia állapotának követésére szolgál. Meg lehet tekinteni a fertőzöttek és a pandémia áldozatainak számát országonként.
Az alkalmazás kezdőoldalán egy összefoglaló grafikon és számlálók látszanak, amik alatt pedig az országok listája látható.
Ebben a listában, ha rákattintunk egy országra, akkor az adott ország részletes statisztikái és saját grafikonjai jönnek be egy külön oldalon.
Az országok listája rendezhető a következő értékek szerint:  

- esetek száma
- mai esetek száma
- egymillió főre jutó esetek száma
- halálesetek száma
- mai halálesetek száma

Emellett a lista kereshető is. 

# A használt API
Az adatokat a [disease.sh][api] publikus API-ból kéri le az alkalmazás. Ez egy jól dokumentált API, ami meg tudja adni többek közt a következő adatokat:

- összesített adatok
- egy ország adatai
- az összes ország adata
- országok zászlói

# Platformok

Az alkalmazást tervezem Androidra, webre és Linuxra is lefordítani.
Emellett, mivel van Google Play fejlesztői fiókom, illetve webszerverem, így az Androidos alkalmazást tervezem feltölteni a Play Store-ba,
a webes verziót pedig hostolni.



---
title: Specifikáció
subtitle: Flutter alapú szoftverfejlesztés, VIAUAV45
author: Tóth Miklós (FLGIIG)
---

[api]: https://disease.sh/ "disease.sh"