using System;

namespace Equipment
{
    public abstract class EquipmentBase: IEquipment
    {
        protected int yearOfCreation;
        protected int newPrice;

        public EquipmentBase(int year, int price)
        {
            this.yearOfCreation = year;
            this.newPrice = price;
        }
        public int GetAge()
        {
            return DateTime.Today.Year - yearOfCreation;
        }

        public abstract double GetPrice();

        public virtual string GetDescription()
        {
            return "EquipmentBase";
        }
    }
}