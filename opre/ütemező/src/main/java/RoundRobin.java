import java.util.ArrayList;

public class RoundRobin {
    private ArrayList<Task> tasks = new ArrayList<>();
    private Task lastTask = null;
    private int taskRunTime = 0;
    
    public void addTask(Task task) {
        tasks.add(task);
    }
    
    public Task scheduleTask() {
        Task t = tasks.get(0);

        if (lastTask == t && taskRunTime >= 2){
            tasks.remove(t);
            tasks.add(t);
        }
        
        t = tasks.get(0);

        t.left--;
        if (t != lastTask) {
            taskRunTime = 1;
        } else {
            taskRunTime++;
        }
        if (t.left <= 0) {
            tasks.remove(t);
        }
        
        for (Task ta: tasks){
            if (t != ta){
                ta.wait++;
            }
        }

        lastTask = t;

        return t;
    }
    
    public void waitTasks() {
        if (taskRunTime < 2) {
            taskRunTime = 0;
        }
        
        for(Task t: tasks){
            t.wait++;
        }
    }

    public boolean hasTasks() {
        return !tasks.isEmpty();
    }
}
